import { showAlert } from "./actions";
import { CREATE_POST } from "./types"

const forbidden = ["fuck", "spam", "php"];

export const forbiddenWordsMiddleware = ({dispatch}) => {
    return next => {
        return action => {
            if (action.type === CREATE_POST) {
                const found = forbidden.filter(w => action.payload.title.includes(w.toLowerCase()));
                if (found.length) {
                    return dispatch(showAlert("Вы спамер. Мы Вас не звали. Идите домой"))
                }
            }
            return next(action)
        }
    }
}