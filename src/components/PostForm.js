import React from "react";
import { createPost, showAlert } from "../redux/actions"
import { connect } from "react-redux";
class PostForm extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            title: ""
        }
    }

    submitHandler = (event) => {
        event.preventDefault();

        const { title } = this.state;

        if (!title.trim()) {
            return this.props.showAlert("Название поста не может быть пустым")
        }
        
        const newPost = {
            title,
            id: Date.now().toString()
        }
        this.setState({ title: "" })
        this.props.createPost(newPost)
    }
    
    changeInputHandler = (event) => {
        this.setState(state => ({
            ...state,
            [event.target.name]: event.target.value
        }))
    }
    
    render() {
        return (
            <form onSubmit={this.submitHandler}>

                {
                    this.props.alert ? <div className="alert alert-warning">{this.props.alert}</div> : null
                }

                <div className="mb-3">
                    <label htmlFor="title" className="form-label">Заголовок поста</label>
                    <input 
                        type="text" 
                        className="form-control" 
                        id="title" 
                        value={this.state.title}
                        name="title"
                        onChange={this.changeInputHandler}
                    />
                </div>
                <button type="submit" className="btn btn-success">Создать</button>
            </form>
        )
    }
}

const mapDispatchToProps = {
    createPost,
    showAlert
}

const mapStateToProps = state => ({
    alert: state.app.alert
})

export default connect(mapStateToProps, mapDispatchToProps)(PostForm)