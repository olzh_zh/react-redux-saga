import React from "react";
import { connect } from "react-redux";
import Post from "../components/Post"

function Posts({syncPosts}) {
    if ( !syncPosts.length ) {
        return <p>Постов пока нет</p>
    }
    return syncPosts.map(post => {
        return <Post post={post} key={post.id} />
    })
}

const mapStateToProps = state => {
    return {
        syncPosts: state.posts.posts
    }
}

export default connect(mapStateToProps, null)(Posts)